# Presentation for PhD-Defence for the project *Preliminary Wind Turbine Rotor Design*

The presentation is created using [Reveal.js](https://revealjs.com").

It can be seen at [https://kenloen.pages.windenergy.dtu.dk/phd-defence-presentation/](https://kenloen.pages.windenergy.dtu.dk/phd-defence-presentation/)